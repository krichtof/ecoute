# Session Ecoute 5/03/2019

"Je me rends compte que quand je suis en mode écoute, si je saute pas sur le premier silence venu, et ben la parole on ne me la donne pas ! Du coup, je me rends compte que quand tu n'es pas armé pour prendre la parole, que t'es timide ou quoi ben c'est difficile de prendre la parole."

En apprenant à écouter, tu vas pouvoir trouver tranquillement des stratégies pour trouver l'équilibre avec ton besoin d'expression...

"J'ai besoin de m'exprimer mais je suis embêté avec l'idée de devoir user mon pouvoir pour garder/prendre la parole."

Tu as parlé au _tu_
le _je_ permet
-de rester en responsabilité de ce dont je suis en train de parler
- évite de calquer ton expérience sur l'autre
- permet à l'autre d'être en empathie plus facilement.

On note que le _tu_ permet aussi de se distancier d'émotions/sentiments qui peuvent être vécus comme négatifs.

Avant de partager un retour d'expérience suite à ce que quelqu'un·e te partage quant tu es en posture d'écoute, tu peux
- vérifier que l'autre a fini (c'est bon, tu as fini ? )
- vérifier qu'il·elle est consentant·e pour que tu lui partages ton expérience (est-ce que je peux te partager un truc auquel ça me fait penser ?). Attention à être au clair sur ton intention (suis-je prêt à entendre un non et à ravaler/différer mon enthousiasme de partage ?)

La violence c'est le rythme des autres. 

Curiosité : avoir la curiosité sincère (pas des faits) de ce que l'autre est en train de vivre --> permet la posture de véritable écoute empathique. 

